#!/bin/bash
# Exit if any of the intermediate steps fail
set -e

# Extract "foo" and "baz" arguments from the input into
# FOO and BAZ shell variables.
# jq will ensure that the values are properly quoted
# and escaped for consumption by the shell.
eval "$(jq -r '@sh "KUBECONFIG=\(.kubeconfig)"')"

SECRET=$(kubectl -n kube-system --kubeconfig $KUBECONFIG get secret -o name | grep eks-admin)

jq -n --arg secret "$SECRET" '{"secret":$secret}'