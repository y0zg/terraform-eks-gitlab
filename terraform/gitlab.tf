# Add a project owned by the user
data "gitlab_project" "custom_gitlab_actions" {
    id = var.gitlab-project-id
}

locals {
  admin_token = <<ADMINTOKEN

apiVersion: v1
kind: ServiceAccount
metadata:
  name: eks-admin
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: eks-admin
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: eks-admin
  namespace: kube-system
ADMINTOKEN
}

resource "null_resource" "create-admin-token" {
  provisioner "local-exec" {
    command = "cat <<EOF | kubectl apply --kubeconfig ${local_file.kubeconfig.filename} -f - ${local.admin_token}EOF"
  }
}

data "external" "api_secret_name" {
  program = ["bash", "${path.module}/scripts/get-api-secret-name.sh"]

  query = {
    kubeconfig = "${local_file.kubeconfig.filename}"
  }

  depends_on = [
    null_resource.create-admin-token
  ]
}

data "external" "api_token" {
  program = ["bash", "${path.module}/scripts/get-api-token.sh"]

  query = {
    kubeconfig = "${local_file.kubeconfig.filename}"
    secret = "${data.external.api_secret_name.result.secret}"
  }
}

resource gitlab_project_cluster "vnagy_eks" {
  project                       = data.gitlab_project.custom_gitlab_actions.id
  name                          = "vnagy-eks"
  domain                        = "gc2.nagyv.com"
  enabled                       = true
  kubernetes_api_url            = aws_eks_cluster.cluster.endpoint
  kubernetes_token              = data.external.api_token.result.token
  kubernetes_ca_cert            = base64decode(aws_eks_cluster.cluster.certificate_authority.0.data)
  kubernetes_namespace          = ""
  kubernetes_authorization_type = "rbac"
  environment_scope             = var.cluster-environment
}